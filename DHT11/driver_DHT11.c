#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
//#include <linux/cdev.h>
#include <linux/miscdevice.h>   // misc dev
#include <linux/fs.h>         // file operations
#include <asm/uaccess.h>      // copy to/from user space
#include <linux/wait.h>       // waiting queue
#include <linux/sched.h>      // TASK_INTERRUMPIBLE
#include <linux/delay.h>      // udelay

#include <linux/interrupt.h>
#include <linux/gpio.h>
//#include <linux/gpio/consumer.h>


#define DRIVER_AUTHOR "Andres Rodriguez - DAC"
#define DRIVER_DESC   "Driver para DHT11"

//GPIOS numbers as in BCM RPi

static int GPIO_SENSOR = 6;
module_param(GPIO_SENSOR, int, 0);
MODULE_PARM_DESC(GPIO_SENSOR, "gpio connected to DHT11 data");


/*
 *	file_operations section
 */

/****************************************************************************/
/* DHT11 device file operations                                              */
/****************************************************************************/

static int dht11_open(struct inode *inode, struct file *file)
{
    /* client related data can be allocated here and
       stored in file->private_data */
    return 0;
}

static int dht11_release(struct inode *inode, struct file *file)
{
    /* client related data can be retrived from file->private_data
       and released here */
    return 0;
}


#define MAX 2048
unsigned int val[MAX];
char mibuffer[MAX*2];

int DHT11_RES=0;

DECLARE_WAIT_QUEUE_HEAD(read_queue);


#define DHT11_LOOP_DELAY 4
#define DHT11_uSEC_LIMIT 150
#define DHT11_PULSES 41

int pulses[DHT11_PULSES*2];



static void read_DHT11_func(unsigned long b)
{
    int i,j;
    int ret;
    int count=0;
    int limite;
    unsigned long flags;

    limite= DHT11_uSEC_LIMIT / DHT11_LOOP_DELAY + 1;

    DHT11_RES=0;
	
	gpio_set_value(GPIO_SENSOR,1);
    udelay(40);
    gpio_direction_input(GPIO_SENSOR);
   

    local_irq_save(flags);

    for(i=0; i<MAX; i++)
    {

        if((val[i]= gpio_get_value(GPIO_SENSOR))) count++;
        else count=0;
        if(count>limite) break;
        udelay(DHT11_LOOP_DELAY);

        //valtime[i]=micros();
    }

    local_irq_restore(flags);

    ret=i;
    j=0;
    count=0;
    for(i=0; i<ret; i++)
    {

        if(val[i]!=count)
        {
            count=val[i];
            mibuffer[j++]='\n';
        }
        mibuffer[j++]='0'+val[i];
        //valtime[i]=micros();
    }

    mibuffer[j++]='\n';

    DHT11_RES=j;

    wake_up_interruptible(&read_queue);

    return;
}

static DECLARE_TASKLET(read_DHT11, read_DHT11_func, 0);


static ssize_t dht11_read(struct file *file, char __user *buf,
                          size_t count, loff_t *ppos)
{

    int humi=0, temp=0;
    int ret;
    int res=0;
    int CRC=0;
    int pcount;
    int lval;
    int i;
    int j;
    int threshold;

    int data[5] = {0,0,0,0,0};

    if(*ppos!=0)  return 0; // EOF


    DHT11_RES=0;

    gpio_direction_output(GPIO_SENSOR,0);  // init Request
    mdelay(20);

    tasklet_schedule(&read_DHT11);

    ret=wait_event_interruptible(read_queue, DHT11_RES>0);

    if(ret==-ERESTARTSYS) return ret;

    res=DHT11_RES;
    lval=0;
    pcount=0;
    j=0;

    for(i=0; i<res && j< DHT11_PULSES*2; i++)

    {

        if(lval==val[i])
        {
            pcount++;
        }
        else
        {
            pulses[j]=pcount;
            j++;
            lval=val[i];
            pcount=0;
        }

    }

    threshold=0;
    for (i=2; i < DHT11_PULSES*2; i+=2) {
        threshold += pulses[i];
    }
    threshold /= DHT11_PULSES-1;    // mean pulse for 0 = 54 usec
    threshold = threshold *47 /54 +1;  // calculate medium point 24-70 is 47 usec

    for (i=3; i < DHT11_PULSES*2; i+=2) {
        int index = (i-3)/16;
        data[index] <<= 1;
        if (pulses[i] >= threshold) {
            // One bit for long pulse.
            data[index] |= 1;
        }
        // Else zero bit for short pulse.
    }

    // Useful debug info:
    //printf("Data: 0x%x 0x%x 0x%x 0x%x 0x%x\n", data[0], data[1], data[2], data[3], data[4]);

    // Verify checksum of received data.

    humi = data[0];
    temp = data[2];

    CRC=!(data[4] == ((data[0] + data[1] + data[2] + data[3]) & 0xFF));




    if(!CRC)
    {
        sprintf(mibuffer+res,"%3d.0%% %3d.0C\n",humi, temp);
        res+=14;
    }
    else
    {
        sprintf(mibuffer+res,"READ CRC ERROR");
        res+=14;
    }


    if( res>count) res=count;
    if(copy_to_user(buf,mibuffer,res)) return -EFAULT;


    *ppos+=res;  // avanza puntero, la siguiente lectura será EOF

    return res;
}


static const struct file_operations dht11_fops = {
    .owner	= THIS_MODULE,
    .open	= dht11_open,
    .release= dht11_release,
    .read	= dht11_read,
    /*
        	.poll	= dht11_poll,
        	.mmap	= dht11_mmap,
        	.unlocked_ioctl	= dht11_ioctl,
        */
};

/****************************************************************************/
/* dht11 device struct                                                       */

static struct miscdevice dht11_miscdev = {
    .minor	= MISC_DYNAMIC_MINOR,
    .name	= "dht11",
    .fops	= &dht11_fops,
};




/*****************************************************************************/
/* This functions registers devices, requests GPIOs and configures interrupts */
/*****************************************************************************/


void r_dev_config(void)
{
    int ret;

    /*******************************
     *  register device for DHT11
     *******************************/

    ret = misc_register(&dht11_miscdev);
    if (ret < 0) {
        printk(KERN_ERR "misc_register failed\n");
//		r_cleanup();
        return;
    }

    printk(KERN_NOTICE "  dht11_miscdev.minor   =%d\n", dht11_miscdev.minor);


}


void r_GPIO_config(void)
{


    /*******************************
     *  request and init gpios
     *******************************/


    /// SPEAKER GPIO:
    if (gpio_request(GPIO_SENSOR, "GPIO FOR SPEAKER")) {
        printk("GPIO request faiure: %d  %s\n",GPIO_SENSOR, "GPIO FOR SPEAKER");
        return;
    }

}

void r_GPIO_release(void) {

    gpio_free(GPIO_SENSOR);
    return;
}


void r_dev_release(void) {

    if (dht11_miscdev.this_device)
        misc_deregister(&dht11_miscdev);

}

/****************************************************************************/
/* Module init / cleanup block.                                             */
/****************************************************************************/
int r_init(void) {

    printk(KERN_NOTICE "Hello, loading %s module!\n",KBUILD_MODNAME);


    printk(KERN_NOTICE "DHT11 - devices config\n");
    r_dev_config();

    printk(KERN_NOTICE "DHT11 - GPIO config\n");
    r_GPIO_config();


    return 0;
}



void r_cleanup(void) {

    printk(KERN_NOTICE "DHT11 module cleaning up...\n");
    r_GPIO_release();
    r_dev_release();
    printk(KERN_NOTICE "Done. Bye\n");

    return;
}


module_init(r_init);
module_exit(r_cleanup);


/****************************************************************************/
/* Module licensing/description block.                                      */
/****************************************************************************/
MODULE_LICENSE("GPL");
MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
