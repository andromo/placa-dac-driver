#include <stdio.h>
#include <stdlib.h>

#include <wiringPi.h>


int led[6]= {13,12,14,0,3,2} ; // pins using wiringpi numbers
char *color[6]= {"verde", "verde", "amarillo", "amarillo", "rojo", "rojo"};

int main (void)
{
    int i;

    printf ("Raspberry Pi - prueba leds    \n") ;
    printf ("==============================\n") ;
    printf ("\n") ;

    wiringPiSetup() ;


    for(i=0; i< 6; i++)
        pinMode (led[i], OUTPUT) ;


    for(i=0; i< 6; i++)
    {
        printf("Enciendo led %d en el pin %2d de color %s\n", i, led[i], color[i]);

        digitalWrite(led[i],1);
        delay(1000);

    }

    for(i=0; i< 6; i++)
    {
        digitalWrite(led[i],0);
        delay(1000);
    }

}
