#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>


#define GPIO_data 21

#include <wiringPi.h>

int CRITICAL_INDEX=3;  // initial critical temp
float CRITICAL_TEMP[5]= {21.0f,22.0f,24.0f,26.0f,28.0f};   // array of possible critical temps

pthread_t warning_th;   // thread to make sound

int warning=0;
int speakerON=1;
pthread_mutex_t warning_mutex = PTHREAD_MUTEX_INITIALIZER;  // lock to set sound ON/OFF
pthread_cond_t warning_cond = PTHREAD_COND_INITIALIZER;


#define SENSOR1 "/sys/bus/w1/devices/28-0000059ffb3b/w1_slave"

#define TEMP_ERROR_FILE -1
#define TEMP_ERROR_CRC  -2
#define TEMP_ERROR_DATA -3
#define TEMP_OK          0

int lee_temp(const char *file, float *temp)
{
    FILE *f;
    char ok[256];
    int t;
    int n;
    f=fopen(file,"r");

    if(f==NULL) return TEMP_ERROR_FILE;

    n=fscanf(f, " %*s %*s %*s %*s %*s %*s %*s %*s %*s : %*s %s %*s %*s %*s %*s %*s %*s %*s %*s %*s t=%d ", ok, &t);

    if(n!=2) return TEMP_ERROR_DATA;
    if(strcmp(ok,"YES")!=0) return TEMP_ERROR_CRC;

    fclose(f);

    *temp= (float)t/1000.0f;

    return TEMP_OK;
}



// define GPIO pins using wiringpi numbers
#define BUTTON1	8
#define BUTTON2	9
#define SPEAKER	7
#define GREEN1 	2
#define GREEN2 	3
#define YELLOW1 0
#define YELLOW2 14
#define RED1 	12
#define RED2 	13

#include "play_music.c"

int LED_PINS[6]= {GREEN1,GREEN2,YELLOW1,YELLOW2,RED1,RED2} ;


void init_my_GPIOs()
{
    int i;

    for(i=0; i< 6; i++)
        pinMode (LED_PINS[i], OUTPUT) ;

    pinMode (SPEAKER, OUTPUT) ;
    pinMode (BUTTON1, INPUT);
    pinMode (BUTTON2, INPUT);

}


void play_tone(int freq, float seconds)
{
    int i;
    int pulse=500000/freq;
    int times= (int) (seconds*500000/pulse);
//	printf("pulse: %d, times: %d\n",pulse,times);
    for(i=0; i< times; i++)
    {
        digitalWrite(SPEAKER, 1);
        usleep(pulse);
        digitalWrite(SPEAKER, 0);
        usleep(pulse);
    }
}


void flash_all(int val)
{
    int i;
    for(i=0; i<6; i++)
    {
        digitalWrite(LED_PINS[i], val);
    }
}


void alarma()
{
    int val=0;
    while(digitalRead(BUTTON2))
    {
        flash_all(val=!val);
        play_tone(600,0.25);
        play_tone(200,0.25);

    }
}

void* warning_alarm (void *arg)
{
    int i;
    int freq=200;
    float seconds=0.5;
    int pulse=500000/freq;
    int times= (int) (seconds*500000/pulse);

    struct sched_param sp;
    sp.sched_priority=85;
    pthread_setschedparam(pthread_self(),SCHED_FIFO, &sp);

    while(1)
    {
        pthread_mutex_lock (&warning_mutex) ;
        while(warning==0 || speakerON==0) pthread_cond_wait(&warning_cond, &warning_mutex);
        pthread_mutex_unlock (&warning_mutex) ;
        for(i=0; i< times; i++)
        {
            digitalWrite(SPEAKER, 1);
            delayMicroseconds(pulse);
            digitalWrite(SPEAKER, 0);
            delayMicroseconds(pulse);
        }
        delay(500);

    }
}

void display_temp(float temp, float LOW_T, float HIGH_T, int RES)
{
    int i;
    int led=(int)((temp-LOW_T)*RES/(HIGH_T-LOW_T));
    for(i=1; i<=6; i++)
    {
        if(i<=led)
            digitalWrite(LED_PINS[i-1],1);
        else
            digitalWrite(LED_PINS[i-1],0);
    }
}



void Interrupt_BT2 (void)
{
    static int last_seen=0;
    static int dir=+1;
    int now= millis();   // milisenconds since init
    if(now-last_seen<400) return;  // to avoid rebounds

    pthread_mutex_lock (&warning_mutex);
    speakerON=!speakerON;
    if(speakerON && warning) pthread_cond_broadcast(&warning_cond);
    pthread_mutex_unlock (&warning_mutex);

    if(speakerON==0)
        printf("              BUTTON2 => SPEAKER OFF\n");
    else
        printf("              BUTTON2 => SPEAKER ON\n");

    last_seen=now;
}

void Interrupt_BT1 (void)
{
    static int last_seen=0;
    static int dir=+1;
    int now= millis();   // milisenconds since init
    if(now-last_seen<400) return;  // to avoid rebounds

    if(CRITICAL_INDEX==4) dir=-1;
    if(CRITICAL_INDEX==0) dir=+1;
    CRITICAL_INDEX= (CRITICAL_INDEX+dir);
    printf("              BUTTON1 => NEW CRITICAL T= %4.2fºC\n",CRITICAL_TEMP[CRITICAL_INDEX]);

    last_seen=now;
}

#define MAX 4096
unsigned int elapsed[MAX];
unsigned int valtime[MAX];
unsigned int val[MAX];


int main (void)
{
    float temp;
    int error;
    int i, j, led;
    int data;

    printf ("\n") ;
    printf ("Raspberry Pi - prueba sensor DHT11   \n") ;
    printf ("========================================\n") ;
    printf ("\n") ;
    //printf(" CRITICAL Temp.= %4.2f ºC\n",CRITICAL_TEMP[CRITICAL_INDEX]);
    printf ("\n") ;

    wiringPiSetup();


    //  init_my_GPIOs();

    //playMelodyAck();
    //  playMelodyNak();

    struct sched_param sp;
    sp.sched_priority=90;
    pthread_setschedparam(pthread_self(),SCHED_FIFO, &sp);


    pinMode (GPIO_data, OUTPUT) ;
    pullUpDnControl(GPIO_data,PUD_UP);

    digitalWrite(GPIO_data, 0);
    delay(25);
    digitalWrite(GPIO_data, 1);
    pinMode (GPIO_data,  INPUT) ;
    //unsigned int time, oldtime=micros(),elapsed;
    for(i=0; i<MAX; i++)
    {

        val[i]= digitalRead(GPIO_data);
        udelay(4);
    }

    sp.sched_priority=0;
    pthread_setschedparam(pthread_self(),SCHED_OTHER, &sp);

    unsigned int value=val[0];
    unsigned int vtime=valtime[0];

    int bits[128];
    int durs[128];
    int cont=0;
    for(i=1; i<MAX; i++)
    {
        //printf("   time: %d, interval: %d,  value: %d\n", valtime[i], valtime[i]- valtime[i-1], val[i]);
        if(value!=val[i])
        {
            //printf("value: %d, for: %d usec   index: %d - %d\n",value, valtime[i]-vtime, i, cont);
            bits[cont]=value;
            durs[cont]=valtime[i]-vtime;
            cont++;

            value=val[i];
            vtime=valtime[i];
        }
        //printf("time: %d, interval: %d,  value: %d\n", time[i], time[i]- time[i-1], val[i]);
    }
    i=0;
    while(!(bits[i]==1 && durs[i]>30) && i<cont) i++;
    //printf("encontramos comienzo en %d\n",i);
    int res[40];
    int bytes[5];
    for(j=0; j<5; bytes[j++]=0);
    j=0;
    i=i+1;
    int suma1, suma2;

    int dif01;
    int dif02;
    int dif110;
    int dif111;
    int dif120;
    int dif121;

    int TOL=6;
    int cero=54+24+TOL;
    int uno=54+70+TOL;
    int correcciones=0;
    while(i<cont && j<40)
    {
        char *repar="";
        //0
        dif01=durs[i]-54;
        dif02=durs[i+2]-54;

        dif110=durs[i+1]-24;
        dif111=durs[i+1]-70;

        dif120=durs[i+3]-24;
        dif121=durs[i+3]-70;


        suma1=durs[i]+durs[i+1];
        suma2=durs[i+2]+durs[i+3];
        res[j]=-1;
        printf("%2d: %3d %3d %3d %3d \n",j+1,durs[i], durs[i+1], durs[i+2], durs[i+3]);



        if(abs(dif01)>TOL) // pulso 0 no cuadra
        {
            if(abs(dif110)<TOL && abs(dif111)<TOL && dif01>0 && abs(abs(dif01)-cero)<TOL )
            {
                // tenemos un 0  + 54;
                res[j]=0;
                printf("%2d: %3d (%2d) +%3d (%2d %2d)= %3d (%3d-%3d)   --> %d   +0 en el cero\n",j+1,durs[i], abs(dif01), durs[i+1], abs(dif110),abs(dif111),suma1, 54+24+TOL, 54+70+TOL,res[j]);
                j++;
            }
            if(abs(dif110)<TOL && abs(dif111)<TOL && dif01>0 && abs(abs(dif01)-uno)<TOL )
            {
                // tenemos un 1  + 54;
                res[j]=1;
                printf("%2d: %3d (%2d) +%3d (%2d %2d)= %3d (%3d-%3d)   --> %d   +1 en el cero\n",j+1,durs[i], abs(dif01), durs[i+1], abs(dif110),abs(dif111),suma1, 54+24+TOL, 54+70+TOL,res[j]);
                j++;
            }

            durs[i+1]+=dif01;
            durs[i]-=dif01;
            dif01=0;
            dif110=durs[i+1]-24;
            dif111=durs[i+1]-70;

            repar="corregido pulso 1, cero muy largo";
            correcciones++;
        }



        if(abs(dif110)>TOL && abs(dif111)>TOL) // pulso 1 no cuadra
        {

            if(abs(dif02)<TOL && abs(durs[i+1]-24-54-24)<TOL ) // dos ceros
            {
                // tenemos un 0  + 54;
                res[j]=0;
                printf("%2d: %3d (%2d) +%3d (%2d %2d)= %3d (%3d-%3d)   --> %d   +0 en el uno\n",j+1,durs[i], abs(dif01), durs[i+1], abs(dif110),abs(dif111),suma1, 54+24+TOL, 54+70+TOL,res[j]);
                j++;

                durs[i+1]=24;
                dif110=durs[i+1]-24;
                dif111=durs[i+1]-70;
                correcciones++;

            }

            if(abs(dif02)<TOL && abs(durs[i+1]-70-54-70)<TOL ) // dos unos
            {
                // tenemos un 0  + 54;
                res[j]=1;
                printf("%2d: %3d (%2d) +%3d (%2d %2d)= %3d (%3d-%3d)   --> %d   +0 en el uno\n",j+1,durs[i], abs(dif01), durs[i+1], abs(dif110),abs(dif111),suma1, 54+24+TOL, 54+70+TOL,res[j]);
                j++;

                durs[i+1]=70;
                dif110=durs[i+1]-24;
                dif111=durs[i+1]-70;
                correcciones++;

            }

            if(abs(dif02)<TOL && abs(durs[i+1]-24-54-70)<TOL ) // uno cero, apostamos por una posibilidad
            {
                // tenemos un 0  + 54;
                res[j]=0;
                printf("%2d: %3d (%2d) +%3d (%2d %2d)= %3d (%3d-%3d)   --> %d   +0 en el uno\n",j+1,durs[i], abs(dif01), durs[i+1], abs(dif110),abs(dif111),suma1, 54+24+TOL, 54+70+TOL,res[j]);
                j++;

                durs[i+1]=70;
                dif110=durs[i+1]-24;
                dif111=durs[i+1]-70;
                correcciones++;

            }

            if(durs[i+2]<54-TOL && !(abs(dif120)>TOL && abs(dif121)>TOL) && abs(durs[i+2]+durs[i+1]-24-54)<TOL ) // tenemos un cero corrido
            {
                // tenemos un 0  + 54;
                res[j]=0;

                durs[i+1]=24;
                dif110=durs[i+1]-24;
                dif111=durs[i+1]-70;
                correcciones++;
                durs[i+2]=54;
                repar="corregido pulso 0, pulso muy largo";

            }
            if(durs[i+2]<54-TOL && !(abs(dif120)>TOL && abs(dif121)>TOL) && abs(durs[i+2]+durs[i+1]-70-54)<TOL ) // tenemos un uno corrido
            {
                // tenemos un 0  + 54;
                res[j]=1;

                durs[i+1]=70;
                dif110=durs[i+1]-24;
                dif111=durs[i+1]-70;
                correcciones++;
                durs[i+2]=54;
                repar="corregido pulso 1, pulso muy largo";

            }




        }
        if(durs[i+1]>24+TOL && durs[i+1]<70-TOL && abs(durs[i+2]+durs[i+1]-24-54)<TOL) //era un cero
        {
            // tenemos un 0  + 54;
            res[j]=0;

            durs[i+1]=24;
            dif110=durs[i+1]-24;
            dif111=durs[i+1]-70;
            correcciones++;
            durs[i+2]=54;
            repar="corregido pulso 0, pulso muy largo";
        }

        if(durs[i+1]>70+TOL && abs(durs[i+2]+durs[i+1]-70-54)<TOL) //era un uno
        {
            res[j]=1;

            durs[i+1]=70;
            dif110=durs[i+1]-24;
            correcciones++;
            dif111=durs[i+1]-70;
            durs[i+2]=54;
            repar="corregido pulso 1, pulso muy largo";

        }


        if(abs(dif01)<=TOL && abs(dif110)<=TOL)
        {
            res[j]=0;
        }
        if(abs(dif01)<=TOL && abs(dif111)<=TOL)
        {
            res[j]=1;
        }

        if(res<0 && abs(durs[i]+durs[i+1]-54-24) <TOL)
        {
            res[j]=0;
            correcciones++;
        }

        if(res<0 && abs(durs[i]+durs[i+1]-54-70) <TOL)
        {
            res[j]=1;
            correcciones++;
        }


salida:
        printf("%2d: %3d (%2d) +%3d (%2d %2d)= %3d (%3d-%3d)   --> %d   %s\n",j+1,durs[i], abs(dif01), durs[i+1], abs(dif110),abs(dif111),suma1, 54+24+TOL, 54+70+TOL,res[j], repar);
        i+=2;
        j++;

    }

    for(j=0; j<40; j++)
    {
        //printf("value: %d, for: %d usec   index: %d\n",bits[i], durs[i], j);
        bytes[j/8] |= res[j] << (7-(j%8));
    }


    for(i=i+2; j<40 && i<cont; j++, i+=2)
    {
        if(bits[i]!=1) printf("error\n");

        if(durs[i]>30) res[j]=1;
        else res[j]=0;
        //printf("value: %d, for: %d usec   index: %d\n",bits[i], durs[i], j);
        bytes[j/8] |= res[j] << (7-(j%8));
    }

    int crc=bytes[1]+bytes[2]+bytes[3]+bytes[0];
    if(crc==bytes[4])
    {
        printf("CRC OK, %2d correcciones\n",correcciones);
    }
    else
        printf("CRC FAIL: %d = %d\n",crc, bytes[4]);


    printf("Humedad: %d.%d%%  Temperatura: %d.%dºC \n", bytes[0],bytes[1],bytes[2],bytes[3]);




}
