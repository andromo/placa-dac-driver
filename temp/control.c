/********************************************************
 * PROGRAMA DE CONTROL inicial para Inf. Industrial     *
 *                                                      *
 * Usando threads y sobre la Raspnerry Pi               *
 * Compilar:                                            *
 *    gcc control.c -o control -lwiringPi -pthread      *
 * ******************************************************/

#include <stdio.h>
#include <stdlib.h>

/*********************************
 * Librería de funciones para leer temperatura
 * *****************************/
#include "templib.c"


/*********************************
 * Librería de funciones para manejar threads
 * *****************************/
#include "threads.c"


#include <wiringPi.h>

// define GPIO pins using wiringpi numbers
#define BUTTON1	8
#define BUTTON2	9
#define SPEAKER	7
#define GREEN1 	2
#define GREEN2 	3
#define YELLOW1 0
#define YELLOW2 14
#define RED1 	12
#define RED2 	13

/// para encender los leds
int leds[6]= {GREEN1,GREEN2,YELLOW1,YELLOW2,RED1,RED2} ;

/// para leer temperatura
char *nombre_sensor;

/// variables
float temperatura=-1000.0;
float temp_critica= 20.0;
int boton1=0;
int boton2=0;
int alarma=0;
int altavoz=1;

/// condiciones de control
tipo_condicion *sonido, *sensor, *datos, *boton;

/// prioridades de los threads  1-99  (99 es mayor)
#define PRIORIDAD_LEER 		50
#define PRIORIDAD_DECIDIR 	50
#define PRIORIDAD_SONAR		50
#define PRIORIDAD_VISUALIZAR 50
#define PRIORIDAD_ENTRADA 	50

void inicia_GPIOs()
{
    int i;

    for(i=0; i< 6; i++)
        pinMode (leds[i], OUTPUT) ;

    pinMode (SPEAKER, OUTPUT) ;
    pinMode (BUTTON1, INPUT);
    pinMode (BUTTON2, INPUT);

}

/// thread encargado de leer los botones
void* entrada (void *arg)
{
    establecer_prioridad(PRIORIDAD_ENTRADA);

    while(1)
    {

        espera_condicion_reset(boton);
        printf("se pulsó un botón...\n");

        if(boton1)
        {   // se pulsó el boton1

            boton1=0;
            actualiza_condicion(sonido, 1 ); //activa o desactiva el sonido

        }
        if(boton2)
        {   // se pulsó el boton2
            boton2=0;
            actualiza_condicion(sonido, 0 ); //activa o desactiva el sonido
        }

    }
}

/// thread encargado de hacer sonar la alarma
void* sonar (void *arg)
{
    int i;
    int freq=150;
    float seconds=0.5;
    int pulse=500000/freq;
    int times= (int) (seconds*500000/pulse);

    establecer_prioridad(PRIORIDAD_SONAR);

    while(1)
    {
        espera_condicion(sonido); // si se cumple la condición suena (continua)

        for(i=0; i< times; i++)
        {
            digitalWrite(SPEAKER, 1);
            delayMicroseconds(pulse);
            digitalWrite(SPEAKER, 0);
            delayMicroseconds(pulse);
        }
        delay(500);

    }
}

/// manejador interrupción
void interrupcion_boton1 (void)
{
    boton1=1;
    actualiza_condicion(boton, 1);
}

void interrupcion_boton2 (void)
{
    boton2=1;
    actualiza_condicion(boton, 1);
}

/// función inicial
int main (void)
{
    float temp;
    int error;
    int i, n;

    printf ("\n") ;
    printf ("Raspberry Pi - control temperatura con threads   \n") ;
    printf ("==============================================\n") ;
    printf ("\n") ;
    printf("Temperatura crítica inicial= %4.2f ºC\n", temp_critica);
    printf("Usa CTRL+C para terminar\n");

///  ---------------------------------------------------
    if(num_sensores() < 1)
    {
        printf("no es posible leer temperaturas\n");
        exit(0);
    }

    nombre_sensor = sensor_n(1);

    printf("Trabajaremos con el sensor: %s\n", nombre_sensor);


    ///  ---------------------------------------------------
    wiringPiSetup();

    inicia_GPIOs();

    /// preapara el manejador de la interrupción
    wiringPiISR (BUTTON1, INT_EDGE_FALLING, interrupcion_boton1) ;
    wiringPiISR (BUTTON2, INT_EDGE_FALLING, interrupcion_boton2) ;


    /// inicia condiciones para sincronizar threads
    sonido=inicia_condicion(0);
    sensor=inicia_condicion(0);
    datos=inicia_condicion(0);
    boton=inicia_condicion(0);

    /// este es el thread principal, es "decidir"
    establecer_prioridad(PRIORIDAD_DECIDIR);

    /// lanza otros threads
    inicia_thread(sonar);
    inicia_thread(entrada);


    while(1)
    {

        espera_condicion_reset(sensor); //espera en la condición sensor a que haya una temperatura leída
        // toma decisiones

    }


}
