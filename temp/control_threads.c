/********************************************************
 * PROGRAMA DE CONTROL inicial para Inf. Industrial     *
 *                                                      *
 * Usando threads y sobre la Raspnerry Pi               *
 * Compilar:                                            *
 *    gcc control.c -o control -lwiringPi -pthread      *
 * ******************************************************/

#include <stdio.h>
#include <stdlib.h>

/*********************************
 * Librería de funciones para leer temperatura
 * *****************************/
#include "templib.c"


/*********************************
 * Librería de funciones para manejar threads
 * *****************************/
#include "threads.c"


#include <wiringPi.h>

// define GPIO pins using wiringpi numbers
#define BUTTON1	8
#define BUTTON2	9
#define SPEAKER	7
#define GREEN1 	2
#define GREEN2 	3
#define YELLOW1 0
#define YELLOW2 14
#define RED1 	12
#define RED2 	13


int LED_PINS[6]= {GREEN1,GREEN2,YELLOW1,YELLOW2,RED1,RED2} ;



#define PRIORIDAD_ALTAVOZ 50
#define PRIORIDAD_SENSOR  60


int CRITICAL_INDEX=3;  // initial critical temp
float CRITICAL_TEMP[5]= {21.0f,22.0f,24.0f,26.0f,28.0f};   // array of possible critical temps

int warning=0;
int speakerON=1;

tipo_condicion *sonido, *sensor, *display, *boton;

float temperatura=-1000.0f;
char *nombre_sensor;


int boton1=0;
int boton2=0;




void init_my_GPIOs()
{
    int i;

    for(i=0; i< 6; i++)
        pinMode (LED_PINS[i], OUTPUT) ;

    pinMode (SPEAKER, OUTPUT) ;
    pinMode (BUTTON1, INPUT);
    pinMode (BUTTON2, INPUT);

}


void display_temp(float temp, float LOW_T, float HIGH_T, int RES)
{
    int i;
    int led=(int)((temp-LOW_T)*RES/(HIGH_T-LOW_T));
    for(i=1; i<=6; i++)
    {
        if(i<=led)
            digitalWrite(LED_PINS[i-1],1);
        else
            digitalWrite(LED_PINS[i-1],0);
    }
}



void* entrada (void *arg)
{
    static int dir= +1;
    establecer_prioridad(PRIORIDAD_SENSOR);
    while(1)
    {

        espera_condicion_reset(boton);
        printf("se pulsó un boton\n");

        if(boton1)
        {
            if(CRITICAL_INDEX==4) dir=-1;
            if(CRITICAL_INDEX==0) dir=+1;
            CRITICAL_INDEX= (CRITICAL_INDEX+dir);
            printf("              BUTTON1 => NEW CRITICAL T= %4.2fºC\n",CRITICAL_TEMP[CRITICAL_INDEX]);
            boton1=0;
        }
        if(boton2)
        {
            speakerON=!speakerON;
            actualiza_condicion(sonido, (speakerON && warning));

            if(speakerON==0)
                printf("              BUTTON2 => SPEAKER OFF\n");
            else
                printf("              BUTTON2 => SPEAKER ON\n");
            boton2=0;
        }

    }
}

void* decide (void *arg)
{
    establecer_prioridad(PRIORIDAD_SENSOR);
    while(1)
    {

        espera_condicion_reset(sensor);
        actualiza_condicion(display, 1);

        if(temperatura > CRITICAL_TEMP[CRITICAL_INDEX])
        {
            if( warning==0) printf("      ==> Temp. above critical level!!  > %4.2f ºC\n",CRITICAL_TEMP[CRITICAL_INDEX]);

            warning=1;
            actualiza_condicion(sonido, (speakerON && warning));

        }
        else
        {
            warning=0;
            actualiza_condicion(sonido, (speakerON && warning));
        }

    }
}


void* visualiza (void *arg)
{
    establecer_prioridad(PRIORIDAD_SENSOR);
    while(1)
    {

        espera_condicion_reset(display);
        display_temp(temperatura, 20.0, 30.0, 6);
        //printf("\33[1A\r");
        printf("Temperatura actual: %4.2f ºC  SPEAKER: %d   WARNING: %d\n",temperatura, speakerON, warning);


    }
}


void* read_sensor (void *arg)
{
    establecer_prioridad(PRIORIDAD_SENSOR);
    while(1)
    {

        int error=lee_temp(nombre_sensor, &temperatura);

        if(error)
            printf("Error de lectura en temperatura: %d\n",error);

        actualiza_condicion(sensor, 1);
        delay(500);
    }
}


void* warning_alarm (void *arg)
{
    int i;
    int freq=150;
    float seconds=0.5;
    int pulse=500000/freq;
    int times= (int) (seconds*500000/pulse);

    establecer_prioridad(PRIORIDAD_ALTAVOZ);

    while(1)
    {
        espera_condicion(sonido);

        for(i=0; i< times; i++)
        {
            digitalWrite(SPEAKER, 1);
            delayMicroseconds(pulse);
            digitalWrite(SPEAKER, 0);
            delayMicroseconds(pulse);
        }
        delay(500);

    }
}


void Interrupt_BT2 (void)
{
    boton2=1;
    actualiza_condicion(boton,1);
}

void Interrupt_BT1 (void)
{
    boton1=1;
    actualiza_condicion(boton, 1);
}

int main (void)
{
    float temp;
    int error;
    int i, j, led;

    printf ("\n") ;
    printf ("Raspberry Pi - prueba leds termometro   \n") ;
    printf ("========================================\n") ;
    printf ("\n") ;
    printf(" CRITICAL Temp.= %4.2f ºC\n",CRITICAL_TEMP[CRITICAL_INDEX]);
    printf ("\n") ;

///  ---------------------------------------------------
    int n;




    n = num_sensores();

    printf("Hay %d sensores de temperatura\n", n);

    if(n==0)
    {
        printf("no es posible leer temperaturas\n");
        exit(0);
    }

    nombre_sensor = sensor_n(1);

    printf("Trabajaremos con el primero: %s\n", nombre_sensor);


    ///  ---------------------------------------------------
    wiringPiSetup();

    init_my_GPIOs();



    //playMelodyAck();
    // playMelodyNak();

    wiringPiISR (BUTTON2, INT_EDGE_FALLING, Interrupt_BT2) ;
    wiringPiISR (BUTTON1, INT_EDGE_FALLING, Interrupt_BT1) ;



    sonido=inicia_condicion(0);
    sensor=inicia_condicion(0);
    display=inicia_condicion(0);
    boton=inicia_condicion(0);

    inicia_thread(read_sensor);
    inicia_thread(visualiza);
    inicia_thread(warning_alarm);
    inicia_thread(entrada);

    // thread principal decide
    establecer_prioridad(PRIORIDAD_SENSOR);
    while(1)
    {

        espera_condicion_reset(sensor);
        actualiza_condicion(display, 1);

        if(temperatura > CRITICAL_TEMP[CRITICAL_INDEX])
        {
            if( warning==0) printf("      ==> Temp. above critical level!!  > %4.2f ºC\n",CRITICAL_TEMP[CRITICAL_INDEX]);

            warning=1;
            actualiza_condicion(sonido, (speakerON && warning));

        }
        else
        {
            warning=0;
            actualiza_condicion(sonido, (speakerON && warning));
        }

    }


}
