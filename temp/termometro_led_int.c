#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>



#include <wiringPi.h>

int CRITICAL_INDEX=3;  // initial critical temp
float CRITICAL_TEMP[5]= {21.0f,22.0f,24.0f,26.0f,28.0f};   // array of possible critical temps

pthread_t warning_th;   // thread to make sound

int warning=0;
int speakerON=1;
pthread_mutex_t warning_mutex = PTHREAD_MUTEX_INITIALIZER;  // lock to set sound ON/OFF
pthread_cond_t warning_cond = PTHREAD_COND_INITIALIZER;





#include <string.h>

char *sensor_file_name(char *config_file)
{
    FILE *f;
    char name[256];
    char filename[1024];
    f=fopen(config_file,"r");

    if(f==NULL) return NULL;

    if(fscanf(f, " %s ", name)!=1) return NULL;

    sprintf(filename, "/sys/bus/w1/devices/%s/w1_slave",name);

    return strdup(filename);

}

#define TEMP_ERROR_FILE -1
#define TEMP_ERROR_CRC  -2
#define TEMP_ERROR_DATA -3
#define TEMP_OK          0

int lee_temp(const char *file, float *temp)
{
    FILE *f;
    char ok[256];
    int t;
    int n;
    f=fopen(file,"r");

    if(f==NULL) return TEMP_ERROR_FILE;

    n=fscanf(f, " %*s %*s %*s %*s %*s %*s %*s %*s %*s : %*s %s %*s %*s %*s %*s %*s %*s %*s %*s %*s t=%d ", ok, &t);

    if(n!=2) return TEMP_ERROR_DATA;
    if(strcmp(ok,"YES")!=0) return TEMP_ERROR_CRC;

    fclose(f);

    *temp= (float)t/1000.0f;

    return TEMP_OK;
}



// define GPIO pins using wiringpi numbers
#define BUTTON1	8
#define BUTTON2	9
#define SPEAKER	7
#define GREEN1 	2
#define GREEN2 	3
#define YELLOW1 0
#define YELLOW2 14
#define RED1 	12
#define RED2 	13

#include "play_music.c"

int LED_PINS[6]= {GREEN1,GREEN2,YELLOW1,YELLOW2,RED1,RED2} ;


void init_my_GPIOs()
{
    int i;

    for(i=0; i< 6; i++)
        pinMode (LED_PINS[i], OUTPUT) ;

    pinMode (SPEAKER, OUTPUT) ;
    pinMode (BUTTON1, INPUT);
    pinMode (BUTTON2, INPUT);

}


void play_tone(int freq, float seconds)
{
    int i;
    int pulse=500000/freq;
    int times= (int) (seconds*500000/pulse);
//	printf("pulse: %d, times: %d\n",pulse,times);
    for(i=0; i< times; i++)
    {
        digitalWrite(SPEAKER, 1);
        usleep(pulse);
        digitalWrite(SPEAKER, 0);
        usleep(pulse);
    }
}


void flash_all(int val)
{
    int i;
    for(i=0; i<6; i++)
    {
        digitalWrite(LED_PINS[i], val);
    }
}


void alarma()
{
    int val=0;
    while(digitalRead(BUTTON2))
    {
        flash_all(val=!val);
        play_tone(600,0.25);
        play_tone(200,0.25);

    }
}

void* warning_alarm (void *arg)
{
    int i;
    int freq=200;
    float seconds=0.5;
    int pulse=500000/freq;
    int times= (int) (seconds*500000/pulse);

    struct sched_param sp;
    sp.sched_priority=85;
    pthread_setschedparam(pthread_self(),SCHED_FIFO, &sp);

    while(1)
    {
        pthread_mutex_lock (&warning_mutex) ;
        while(warning==0 || speakerON==0) pthread_cond_wait(&warning_cond, &warning_mutex);
        pthread_mutex_unlock (&warning_mutex) ;
        for(i=0; i< times; i++)
        {
            digitalWrite(SPEAKER, 1);
            delayMicroseconds(pulse);
            digitalWrite(SPEAKER, 0);
            delayMicroseconds(pulse);
        }
        delay(500);

    }
}

void display_temp(float temp, float LOW_T, float HIGH_T, int RES)
{
    int i;
    int led=(int)((temp-LOW_T)*RES/(HIGH_T-LOW_T));
    for(i=1; i<=6; i++)
    {
        if(i<=led)
            digitalWrite(LED_PINS[i-1],1);
        else
            digitalWrite(LED_PINS[i-1],0);
    }
}



void Interrupt_BT2 (void)
{
    static int last_seen=0;
    static int dir=+1;
    int now= millis();   // milisenconds since init
    if(now-last_seen<400) return;  // to avoid rebounds

    pthread_mutex_lock (&warning_mutex);
    speakerON=!speakerON;
    if(speakerON && warning) pthread_cond_broadcast(&warning_cond);
    pthread_mutex_unlock (&warning_mutex);

    if(speakerON==0)
        printf("              BUTTON2 => SPEAKER OFF\n");
    else
        printf("              BUTTON2 => SPEAKER ON\n");

    last_seen=now;
}

void Interrupt_BT1 (void)
{
    static int last_seen=0;
    static int dir=+1;
    int now= millis();   // milisenconds since init
    if(now-last_seen<400) return;  // to avoid rebounds

    if(CRITICAL_INDEX==4) dir=-1;
    if(CRITICAL_INDEX==0) dir=+1;
    CRITICAL_INDEX= (CRITICAL_INDEX+dir);
    printf("              BUTTON1 => NEW CRITICAL T= %4.2fºC\n",CRITICAL_TEMP[CRITICAL_INDEX]);

    last_seen=now;
}

int main (void)
{
    float temp;
    int error;
    int i, j, led;

    printf ("\n") ;
    printf ("Raspberry Pi - prueba leds termometro   \n") ;
    printf ("========================================\n") ;
    printf ("\n") ;
    printf(" CRITICAL Temp.= %4.2f ºC\n",CRITICAL_TEMP[CRITICAL_INDEX]);
    printf ("\n") ;

    wiringPiSetup();

    init_my_GPIOs();

    //playMelodyAck();
    playMelodyNak();



    wiringPiISR (BUTTON2, INT_EDGE_FALLING, &Interrupt_BT2) ;
    wiringPiISR (BUTTON1, INT_EDGE_FALLING, &Interrupt_BT1) ;


    //  pthread_mutex_lock (&warning_mutex) ;

    pthread_create (&warning_th, NULL, warning_alarm, NULL) ;


    char*SENSOR1=sensor_file_name("device");

    while(1)
    {
        error=lee_temp(SENSOR1, &temp);

        if(error)
            printf("Error de lectura en temperatura: %d\n",error);

        printf("Temperatura actual: %4.2f ºC  SPEAKER: %d   WARNING: %d\n",temp, speakerON, warning);

        display_temp(temp, 20.0, 30.0, 6);

        if(temp > CRITICAL_TEMP[CRITICAL_INDEX])
        {
            if( warning==0) printf("      ==> Temp. above critical level!!  > %4.2f ºC\n",CRITICAL_TEMP[CRITICAL_INDEX]);


            pthread_mutex_lock (&warning_mutex);
            warning=1;
            if(speakerON) pthread_cond_broadcast(&warning_cond);
            pthread_mutex_unlock (&warning_mutex);
        }
        else
        {
            pthread_mutex_lock (&warning_mutex);
            warning=0;
            pthread_mutex_unlock (&warning_mutex);
            delay(500);
        }

    }



}
