#include <stdio.h>
#include <stdlib.h>


/*********************************
 * Librería de funciones para leer temperatura
 * *****************************/
#include "templib.c"

/*********************************
 * función principal
 * *****************************/

main()
{

    int n;
    char *nombre_sensor;

    int error;
    float temp;

    n = num_sensores();

    printf("Hay %d sensores de temperatura\n", n);

    if(n==0)
    {
        printf("no es posible leer temperaturas\n");
        exit(0);
    }

    nombre_sensor = sensor_n(1);

    printf("Trabajaremos con el primero: %s\n", nombre_sensor);


    error = lee_temp(nombre_sensor, &temp);

    if(error)
        printf("Error de lectura en temperatura: %d\n",error);
    else
        printf("Temperatura actual: %4.2f ºC\n",temp);
}
