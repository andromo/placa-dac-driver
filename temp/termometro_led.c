#include <stdio.h>
#include <stdlib.h>

#include <wiringPi.h>


#define SENSOR1 "/sys/bus/w1/devices/28-0000059ffb3b/w1_slave"

#define TEMP_ERROR_FILE -1
#define TEMP_ERROR_CRC  -2
#define TEMP_ERROR_DATA -3
#define TEMP_OK          0

int lee_temp(const char *file, float *temp)
{
    FILE *f;
    char ok[256];
    int t;
    int n;
    f=fopen(file,"r");

    if(f==NULL) return TEMP_ERROR_FILE;

    n=fscanf(f, " %*s %*s %*s %*s %*s %*s %*s %*s %*s : %*s %s %*s %*s %*s %*s %*s %*s %*s %*s %*s t=%d ", ok, &t);

    if(n!=2) return TEMP_ERROR_DATA;
    if(strcmp(ok,"YES")!=0) return TEMP_ERROR_CRC;

    fclose(f);

    *temp= (float)t/1000.0f;

    return TEMP_OK;
}

// define GPIO pins using wiringpi numbers
#define BUTTON1	8
#define BUTTON2	9
#define SPEAKER	7
#define GREEN1 	2
#define GREEN2 	3
#define YELLOW1 0
#define YELLOW2 14
#define RED1 	12
#define RED2 	13


int LED_PINS[6]= {GREEN1,GREEN2,YELLOW1,YELLOW2,RED1,RED2} ;


void init_my_GPIOs()
{
    int i;

    for(i=0; i< 6; i++)
        pinMode (LED_PINS[i], OUTPUT) ;

    pinMode (SPEAKER, OUTPUT) ;
    pinMode (BUTTON1, INPUT);
    pinMode (BUTTON2, INPUT);

}


void play_tone(int freq, float seconds)
{
    int i;
    int pulse=1000000/freq;
    int times= (int) (seconds*1000000/(pulse*2));
//	printf("pulse: %d, times: %d\n",pulse,times);
    for(i=0; i< times; i++)
    {
        digitalWrite(SPEAKER, 1);
        usleep(pulse);
        digitalWrite(SPEAKER, 0);
        usleep(pulse);
    }
}


void flash_all(int val)
{
    int i;
    for(i=0; i<6; i++)
    {
        digitalWrite(LED_PINS[i], val);
    }
}


void alarma()
{
    int val=0;
    while(digitalRead(BUTTON2))
    {
        flash_all(val=!val);
        play_tone(600,0.25);
        play_tone(200,0.25);

    }
}


void display_temp(float temp, float LOW_T, float HIGH_T, int RES)
{
    int i;
    int led=(int)((temp-LOW_T)*RES/(HIGH_T-LOW_T));
    for(i=1; i<=6; i++)
    {
        if(i<=led)
            digitalWrite(LED_PINS[i-1],1);
        else
            digitalWrite(LED_PINS[i-1],0);
    }
}

int main (void)
{
    float temp;
    int error;
    int i, j, led;

    printf ("Raspberry Pi - prueba leds termometro   \n") ;
    printf ("========================================\n") ;
    printf ("\n") ;

    wiringPiSetup() ;

    init_my_GPIOs();



    while(1)
    {
        error=lee_temp(SENSOR1, &temp);

        if(error)
            printf("Error de lectura en temperatura: %d\n",error);

        printf("Temperatura actual: %4.2f ºC\n",temp);

        display_temp(temp, 20.0, 30.0, 6);

        if(temp>28.5f)
            alarma();
        else
            usleep(500000);

    }



}
