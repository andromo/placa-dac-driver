#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>


#define GPIO_data 22

#include <wiringPi.h>


#define MAX 2048
unsigned int val[MAX];
char mibuffer[MAX*2];
#define DHT11_LOOP_DELAY 4    // vamos a muestrear cada 4 us
#define DHT11_uSEC_LIMIT 150  // los pulsos serán como máximo de 150us
#define DHT11_PULSES 41       // número de pulsos que vamos a guardar

int pulses[DHT11_PULSES*2];   // cada pulso tiene duración en alta y baja



int main (void)
{
    // float temp;
    int error;
    int i, j, led,ret;
    int data[5] = {0,0,0,0,0};
    int count=0;
    int limite;
    int res,test_val,humi,temp;


    limite= DHT11_uSEC_LIMIT / DHT11_LOOP_DELAY + 1;


    printf ("\n") ;
    printf ("Raspberry Pi - prueba sensor DHT11   \n") ;
    printf ("========================================\n") ;
    printf ("\n") ;

    // init library
    wiringPiSetup();


    /*** set high RT prio ***/
    struct sched_param sp;
    sp.sched_priority=90;
    pthread_setschedparam(pthread_self(),SCHED_FIFO, &sp);

    //we are going to write in GPIO
    pinMode (GPIO_data, OUTPUT) ;
    pullUpDnControl(GPIO_data,PUD_UP);

    // set 0 for 20ms
    digitalWrite(GPIO_data, 0);
    delay(20);

    // set 1 for 40us
    digitalWrite(GPIO_data, 1);
    delayMicroseconds(40);

    // now we are going to read from GPIO the sensor response
    pinMode (GPIO_data,  INPUT) ;

    // en val vamos a muestrear cada 4us que nos llega por la entrada, terminademos cuando encontremos
    // un nivel alto que dure más de 150us

    for(i=0; i<MAX; i++)
    {

        if((val[i]= digitalRead(GPIO_data))) count++;   //contamos cuantas veces se repite nivel HIGH
        else count=0;
        if(count>limite) break; // terminamos al encontrar un pulso HIGH más de 150us
        delayMicroseconds(DHT11_LOOP_DELAY); // muestreamos cada 4us
    }

    // sensor response captured in val[]

    /*** set prio back to normal ***/
    sp.sched_priority=0;
    pthread_setschedparam(pthread_self(),SCHED_OTHER, &sp);

    // ahora voy a pintar lo que hemos capturado, para poder depurar visualmente

    ret=i;
    j=0;
    test_val=0;
    for(i=0; i<ret; i++)
    {

        if(val[i]!=test_val)
        {
            test_val=val[i];
            mibuffer[j++]='\n';
        }
        mibuffer[j++]='0'+val[i];

    }

    mibuffer[j++]='\n';
    res=j;
    printf("%s",mibuffer);


    // vamos a contar la longitud de cada nivel
    test_val=0;
    count=0;
    j=0;

    for(i=0; i<res && j< DHT11_PULSES*2; i++)

    {

        if(test_val==val[i])
        {
            count++;
        }
        else
        {
            pulses[j]=count;
            j++;
            test_val=val[i];
            count=0;
        }

    }

    // calculamos la duración media de un 0 (54us)
    int threshold=0;
    for (i=2; i < DHT11_PULSES*2; i+=2) {
        threshold += pulses[i];
    }
    threshold /= DHT11_PULSES-1;    // mean pulse for 0 = 54 usec
    threshold = threshold *47 /54 +1;  // calculate medium point 24-70 is 47 usec
    // hemos calculado un threshold para diferenciar la duración de un nivel alto corto (0) y un nivel alto largo (1)

    for (i=3; i < DHT11_PULSES*2; i+=2) {
        int index = (i-3)/16;
        data[index] <<= 1;
        if (pulses[i] >= threshold) {
            // One bit for long pulse.
            data[index] |= 1;
        }
        // Else zero bit for short pulse.
    }

    // Useful debug info:
    //printf("Data: 0x%x 0x%x 0x%x 0x%x 0x%x\n", data[0], data[1], data[2], data[3], data[4]);

    // Verify checksum of received data.

    humi = data[0];
    temp = data[2];

    int CRC=!(data[4] == ((data[0] + data[1] + data[2] + data[3]) & 0xFF));



    printf("%3d.0%% %3d.0C\n",humi, temp);
    if(CRC)
    {
        printf(">>> READ CRC ERROR <<<\n");

    }



}
