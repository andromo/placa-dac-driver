#include <pthread.h>


/********************************
 * Definición del tipo condición tipo_condicion
 * ******************************/
// contiene la variable c, 1 = cumple , 0 = no cumple
// cond es la variable condicional de pthreads
// mutex es el mutex para controlar el acceso exclusivo a la condición
typedef struct
{
    int *c;
    pthread_cond_t *cond;
    pthread_mutex_t *mutex;
} tipo_condicion;


/********************************
 * FUNCIONES DISPONIBLES:
 * ******************************/
// crea una nueva condicion con valor val
tipo_condicion * inicia_condicion( int val);

// espera a que la condición se cumpa (1)
void espera_condicion(tipo_condicion *cond);

// espera a que la condición se cumpa (1) y la pone a 0
void espera_condicion_reset(tipo_condicion *cond);

// pone la condición a 1 o 0
void actualiza_condicion(tipo_condicion *cond, int val);

// crea un nuevo thread que ejecuta la función
pthread_t * inicia_thread(void* (*funcion)());

// pone la prioridad a Tiempo Real con valor n
void establecer_prioridad(int n);

/********************************
 * FUNCIONES (IMPLEMENTACION):
 * ******************************/

tipo_condicion * inicia_condicion( int val)
{
    tipo_condicion *cond;
    cond=(tipo_condicion*)malloc(sizeof(tipo_condicion));
    cond->c= (int*) malloc(sizeof(int));
    cond->cond = (pthread_cond_t *) malloc(sizeof(pthread_cond_t));
    cond->mutex = (pthread_mutex_t *) malloc(sizeof(pthread_mutex_t));

    *(cond->c)= val;
    pthread_cond_init(cond->cond, NULL);
    pthread_mutex_init(cond->mutex, NULL);
    return cond;
}

//-----------------------------------------------

void _actualiza_condicion(int *c, int value, pthread_cond_t *warning_cond, pthread_mutex_t *warning_mutex)
{
    pthread_mutex_lock (warning_mutex);
    *c=value;
    if(*c) pthread_cond_broadcast(warning_cond);
    pthread_mutex_unlock (warning_mutex);
}

//-----------------------------------------------
void _espera_condicion(int *c, pthread_cond_t *warning_cond, pthread_mutex_t *warning_mutex)
{
    pthread_mutex_lock (warning_mutex);
    while(!*c) pthread_cond_wait(warning_cond, warning_mutex);
    pthread_mutex_unlock (warning_mutex);
}

//-----------------------------------------------
void _espera_condicion_r(int *c, pthread_cond_t *warning_cond, pthread_mutex_t *warning_mutex)
{
    pthread_mutex_lock (warning_mutex);
    while(!*c) pthread_cond_wait(warning_cond, warning_mutex);
    *c=0;
    pthread_mutex_unlock (warning_mutex);
}


//-----------------------------------------------
void espera_condicion_reset(tipo_condicion *cond)
{
    _espera_condicion_r(cond->c, cond->cond, cond->mutex);
}
//-----------------------------------------------
void espera_condicion(tipo_condicion *cond)
{
    _espera_condicion(cond->c, cond->cond, cond->mutex);
}
//-----------------------------------------------
void actualiza_condicion(tipo_condicion *cond, int val)
{
    _actualiza_condicion(cond->c, val, cond->cond, cond->mutex);
}
//-----------------------------------------------
void establecer_prioridad(int n)
{
    struct sched_param sp;
    sp.sched_priority=n;
    pthread_setschedparam(pthread_self(),SCHED_FIFO, &sp);
}

//-----------------------------------------------
pthread_t * inicia_thread(void* (*funcion)())
{
    pthread_t *th;
    th=	(pthread_t *) malloc(sizeof(pthread_t ));
    pthread_create (th, NULL, funcion, NULL) ;
    return th;
}
//-----------------------------------------------
void espera_thread (pthread_t * th)
{
    pthread_join( *th, NULL);
}
