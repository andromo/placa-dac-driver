#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>



#include <wiringPi.h>


// define GPIO pins using wiringpi numbers
#define BUTTON1	8
#define BUTTON2	9
#define SPEAKER	7
#define GREEN1 	2
#define GREEN2 	3
#define YELLOW1 0
#define YELLOW2 14
#define RED1 	12
#define RED2 	13

int LED_PINS[6]= {GREEN1,GREEN2,YELLOW1,YELLOW2,RED1,RED2} ;

void display(int temp, int LOW_T, int HIGH_T)
{
    int i;
    int led=(int)((temp-LOW_T)*6/(HIGH_T-LOW_T));
    for(i=1; i<=6; i++)
    {
        if(i<=led)
            digitalWrite(LED_PINS[i-1],1);
        else
            digitalWrite(LED_PINS[i-1],0);
    }
}


void toneManual(int pin, int frequency, int duration)
{
    unsigned long period = 1000000/frequency;
    unsigned long length;
    int state = 0;
    for (length = 0; length < (long) duration * 1000; length += period) {
        state = !state;
        digitalWrite(pin, state);
        /* The 50uS correspond to the time the rest of the loop body takes.
         * It seems about right, but has not been tuned precisely for
         * a 16MHz ATMega. */
        delayMicroseconds(period - 50);
    }
}

const int c = 261;
const int d = 294;
const int e = 329;
const int f = 349;
const int g = 391;
const int gS = 415;
const int a = 440;
const int aS = 455;
const int b = 466;
const int cH = 523;
const int cSH = 554;
const int dH = 587;
const int dSH = 622;
const int eH = 659;
const int fH = 698;
const int fSH = 740;
const int gH = 784;
const int gSH = 830;
const int aH = 880;



void beep(int note, int duration)
{
    //Play tone on buzzerPin
    display(note,230,880);
    toneManual(SPEAKER, note, duration);
    display(0,261,880);
    delay(20);
}

void firstSection()
{
    beep(a, 500);
    beep(a, 500);
    beep(a, 500);
    beep(f, 350);
    beep(cH, 150);
    beep(a, 500);
    beep(f, 350);
    beep(cH, 150);
    beep(a, 650);

    delay(500);

    beep(eH, 500);
    beep(eH, 500);
    beep(eH, 500);
    beep(fH, 350);
    beep(cH, 150);
    beep(gS, 500);
    beep(f, 350);
    beep(cH, 150);
    beep(a, 650);

    delay(500);
}

void variant1()
{
    beep(f, 250);
    beep(gS, 500);
    beep(f, 350);
    beep(a, 125);
    beep(cH, 500);
    beep(a, 375);
    beep(cH, 125);
    beep(eH, 650);

    delay(500);
}

void secondSection()
{
    beep(aH, 500);
    beep(a, 300);
    beep(a, 150);
    beep(aH, 500);
    beep(gSH, 325);
    beep(gH, 175);
    beep(fSH, 125);
    beep(fH, 125);
    beep(fSH, 250);

    delay(325);

    beep(aS, 250);
    beep(dSH, 500);
    beep(dH, 325);
    beep(cSH, 175);
    beep(cH, 125);
    beep(b, 125);
    beep(cH, 250);

    delay(350);
}

void variant2()
{
    beep(f, 250);
    beep(gS, 500);
    beep(f, 375);
    beep(cH, 125);
    beep(a, 500);
    beep(f, 375);
    beep(cH, 125);
    beep(a, 650);
    delay(500);
}


void init_my_GPIOs()
{
    int i;

    for(i=0; i< 6; i++)
        pinMode (LED_PINS[i], OUTPUT) ;

    pinMode (SPEAKER, OUTPUT) ;
    pinMode (BUTTON1, INPUT);
    pinMode (BUTTON2, INPUT);

}


void play_tone(int freq, float seconds)
{
    int i;
    int pulse=500000/freq;
    int times= (int) (seconds*500000/pulse);
//	printf("pulse: %d, times: %d\n",pulse,times);
    for(i=0; i< times; i++)
    {
        digitalWrite(SPEAKER, 1);
        usleep(pulse);
        digitalWrite(SPEAKER, 0);
        usleep(pulse);
    }
}



int main (int argc, char** argv)
{

    printf ("\n") ;
    printf ("Raspberry Pi - prueba de reproducción de música   \n") ;
    printf ("==================================================\n") ;
    printf ("\n") ;

    wiringPiSetup();

    init_my_GPIOs();

    if(argc>1)
    {
        struct sched_param sp;
        sp.sched_priority=85;
        pthread_setschedparam(pthread_self(),SCHED_FIFO, &sp);
    }

    //Play first section
    firstSection();

    //Play second section
    secondSection();

    //Variant 1
    variant1();

    //Repeat second section
    secondSection();

    //Variant 2
    variant2();



    exit(0);



}
