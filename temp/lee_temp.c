#include <stdio.h>
#include <stdlib.h>

#define SENSOR1 "/sys/bus/w1/devices/28-0000059ffb3b/w1_slave"
#define TEMP_ERROR_FILE -1
#define TEMP_ERROR_CRC  -2
#define TEMP_ERROR_DATA -3
#define TEMP_OK          0

int lee_temp(const char *file, float *temp)
{
    FILE *f;
    char ok[256];
    int t;
    int n;
    f=fopen(file,"r");

    if(f==NULL) return TEMP_ERROR_FILE;

    n=fscanf(f, " %*s %*s %*s %*s %*s %*s %*s %*s %*s : %*s %s ", ok);

    if(n!=1) return TEMP_ERROR_DATA;
    if(strcmp(ok,"YES")!=0) return TEMP_ERROR_CRC;

    n=fscanf(f, " %*s %*s %*s %*s %*s %*s %*s %*s %*s t=%d ",&t);

    if(n!=1) return TEMP_ERROR_DATA;

    fclose(f);

    *temp= (float)t/1000.0f;

    return TEMP_OK;
}

main()
{
    int error;
    float temp;
    error=lee_temp(SENSOR1, &temp);
    if(error)
        printf("Error de lectura en temperatura: %d\n",error);
    else
        printf("Temperatura actual: %4.2f ºC\n",temp);
}
