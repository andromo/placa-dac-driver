#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
//#include <linux/cdev.h>
#include <linux/miscdevice.h>   // misc dev
#include <linux/fs.h>         // file operations
#include <asm/uaccess.h>      // copy to/from user space
#include <linux/wait.h>       // waiting queue
#include <linux/sched.h>      // TASK_INTERRUMPIBLE
#include <linux/delay.h>      // udelay

#include <linux/interrupt.h>
#include <linux/gpio.h>
//#include <linux/gpio/consumer.h>


#define DRIVER_AUTHOR "Andres Rodriguez - DAC"
#define DRIVER_DESC   "Driver para placa lab. DAC Rpi"

//GPIOS numbers as in BCM RPi

#define GPIO_BUTTON1 2
#define GPIO_BUTTON2 3

#define GPIO_SPEAKER 4

#define GPIO_GREEN1  27
#define GPIO_GREEN2  22
#define GPIO_YELLOW1 17
#define GPIO_YELLOW2 11
#define GPIO_RED1    10
#define GPIO_RED2    9

static int LED_GPIOS[]= {GPIO_GREEN1,GPIO_GREEN2,GPIO_YELLOW1,GPIO_YELLOW2,GPIO_RED1,GPIO_RED2} ;

static char *led_desc[]= {"GPIO_GREEN1","GPIO_GREEN2","GPIO_YELLOW1","GPIO_YELLOW2","GPIO_RED1","GPIO_RED2"} ;

static int ms_debounce = 250;
module_param(ms_debounce, int, 0);
MODULE_PARM_DESC(ms_debounce, "number of miliseconds used for button interruption debouncing");


/*
 *	file_operations section
 */
 
/****************************************************************************/
/* LEDs device file operations                                              */
/****************************************************************************/

static int leds_open(struct inode *inode, struct file *file)
{
    /* client related data can be allocated here and
       stored in file->private_data */
    return 0;
}

static int leds_release(struct inode *inode, struct file *file)
{
    /* client related data can be retrived from file->private_data
       and released here */
    return 0;
}


static ssize_t leds_write(struct file *file, const char __user *buf,
                          size_t count, loff_t *ppos)
{
    int val;
    char ch;
    int i;

    if (copy_from_user( &ch, buf, 1 )) {
        return -EFAULT;
    }

    val=(int)ch;
    printk( " valor recibido: %d\n",val);

    if((val >> 6) & 1) // set
    {
        for(i=0; i<6; i++) if((val >> i) & 1) gpio_set_value(LED_GPIOS[i], 1);
        return 1;
    }

    if((val >> 7) & 1)  // clear
    {
        for(i=0; i<6; i++) if((val >> i) & 1) gpio_set_value(LED_GPIOS[i], 0);
        return 1;
    }

    for(i=0; i<6; i++) gpio_set_value(LED_GPIOS[i], (val >> i) & 1);


    return 1;
}

static ssize_t leds_read(struct file *file, char __user *buf,
                         size_t count, loff_t *ppos)
{
    int val;
    char ch;
    int i;
    ch=0;
    if(*ppos==0) *ppos+=1;
    else return 0;
    for(i=0; i<6; i++)
    {
        val=gpio_get_value(LED_GPIOS[i]);
        ch= ch | (val << i);
    }
    if(copy_to_user(buf,&ch,1)) return -EFAULT;

    return 1;
}


static const struct file_operations leds_fops = {
    .owner	= THIS_MODULE,
    .open	= leds_open,
    .release= leds_release,
    .write	= leds_write,
    .read	= leds_read,
    /*
        	.poll	= leds_poll,
        	.mmap	= leds_mmap,
        	.unlocked_ioctl	= leds_ioctl,
        */
};

/****************************************************************************/
/* LEDs device struct                                                       */

static struct miscdevice leds_miscdev = {
    .minor	= MISC_DYNAMIC_MINOR,
    .name	= "leds",
    .fops	= &leds_fops,
};



/****************************************************************************/
/* BUTTONs device file operations                                           */
/****************************************************************************/
#define MAX_BUT_BUF 1024
static char buttons_buf[MAX_BUT_BUF];
static int buttons_pos=0;

static int buttons_open(struct inode *inode, struct file *file)
{
    /* client related data can be allocated here and
       stored in file->private_data */
    return 0;
}

static int buttons_release(struct inode *inode, struct file *file)
{
    /* client related data can be retrived from file->private_data
       and released here */
    return 0;
}

DECLARE_WAIT_QUEUE_HEAD(read_queue);

static ssize_t buttons_read(struct file *file, char __user *buf,
                         size_t count, loff_t *ppos)
{
    int ret;
    
    if(buttons_pos==0) 
    {
    ret=wait_event_interruptible(read_queue, buttons_pos>0);
    if(ret==-ERESTARTSYS) return ret;
   }
    
 
     
  *ppos+=1;
  buttons_pos--;
  if(copy_to_user(buf,buttons_buf+buttons_pos,1)) return -EFAULT;
  return 1;
}


static const struct file_operations buttons_fops = {
    .owner	= THIS_MODULE,
    .open	= buttons_open,
    .release= buttons_release,
    
    .read	= buttons_read,
    
};


/****************************************************************************/
/* BUTTONs device struct                                                    */

static struct miscdevice buttons_miscdev = {
    .minor	= MISC_DYNAMIC_MINOR,
    .name	= "buttons",
    .fops	= &buttons_fops,
};


/****************************************************************************/
/* SPEAKER device file operations                                           */
/****************************************************************************/

static struct timer_list speaker_timer;
static int semitonej=0;
static int soundendj=0;

static void speaker_timer_func (unsigned long data)
{
	static int buzzer_state=0;
		buzzer_state=!buzzer_state;
		gpio_set_value(GPIO_SPEAKER, buzzer_state);
		if(jiffies+semitonej < soundendj) 
			mod_timer(&speaker_timer, jiffies + semitonej);
	
}

static DEFINE_TIMER(speaker_timer, speaker_timer_func, 0, 0);



static void spk_tasklet_func(unsigned long b)
{
	int buzzer_state=0;
	while(jiffies < soundendj) 
	{
	buzzer_state=!buzzer_state;
	gpio_set_value(GPIO_SPEAKER, buzzer_state);
	udelay(semitonej);
	}
	
}

static DECLARE_TASKLET(spk_tasklet, spk_tasklet_func, 0); 

static int speaker_open(struct inode *inode, struct file *file)
{
    /* client related data can be allocated here and
       stored in file->private_data */
    return 0;
}

static int speaker_release(struct inode *inode, struct file *file)
{
    /* client related data can be retrived from file->private_data
       and released here */
    return 0;
}

static char speaker_buffer[1024];

static ssize_t speaker_write(struct file *file, const char __user *buf,
                          size_t count, loff_t *ppos)
{
    
    int freq, dur;
    int buzzer_state=0;
    
	if(count>1024) return -EFAULT;
	
    if (copy_from_user( speaker_buffer, buf, count )) {
        return -EFAULT;
    }
	sscanf(speaker_buffer," %d %d ", &freq, &dur);
	
    printk( "  valor recibido speaker: %d , %d\n", freq, dur);
    // set timer values
    semitonej=(500000/freq);
    printk( "    semitone in uSec: %d \n", semitonej);
  
    soundendj=(dur*HZ/1000)+jiffies+1;
   
    tasklet_schedule(&spk_tasklet);
    
    /*
	while(jiffies < soundendj) 
	{
	buzzer_state=!buzzer_state;
	gpio_set_value(GPIO_SPEAKER, buzzer_state);
	udelay(semitonej);
	}
	* */
    // start timer
  //  mod_timer(&speaker_timer,jiffies+semitonej);

    return count;
}


static const struct file_operations speaker_fops = {
    .owner	= THIS_MODULE,
    .open	= speaker_open,
    .release= speaker_release,
    .write	= speaker_write,
    /*
    .read	= speaker_read,
    */
};


/****************************************************************************/
/* SPEAKER device struct                                                    */

static struct miscdevice speaker_miscdev = {
    .minor	= MISC_DYNAMIC_MINOR,
    .name	= "speaker",
    .fops	= &speaker_fops,
};



/****************************************************************************/
/* Interrupts variables block                                               */
/****************************************************************************/
short int irq_BUTTON1    = 0;
short int irq_BUTTON2    = 0;

// text below will be seen in 'cat /proc/interrupt' command
#define GPIO_BUTTON1_DESC           "Boton 1"
#define GPIO_BUTTON2_DESC           "Boton 2"

// below is optional, used in more complex code, in our case, this could be
// NULL
#define GPIO_BUTTON1_DEVICE_DESC    "Placa lab. DAC"
#define GPIO_BUTTON2_DEVICE_DESC    "Placa lab. DAC"

//timer for debouncing
//static struct timer_list debounce1_timer;

static void debounce_timer_func (unsigned long data)
{
	if(data==1) enable_irq(irq_BUTTON1); else  enable_irq(irq_BUTTON2); 	
}

static DEFINE_TIMER(debounce1_timer, debounce_timer_func, 0, 1);
static DEFINE_TIMER(debounce2_timer, debounce_timer_func, 0, 2);

/// Tasklet function for interrupt handling

static void int_tasklet_func(unsigned long b)
{
	buttons_buf[buttons_pos]=(char)b;
	buttons_pos++;
	wake_up_interruptible(&read_queue);
	if(((char)b)=='1')
	mod_timer(&debounce1_timer,jiffies+ ms_debounce * HZ / 1000);
	else
	mod_timer(&debounce2_timer,jiffies+ ms_debounce * HZ / 1000);
}

static DECLARE_TASKLET(int_botton1_tasklet, int_tasklet_func, (unsigned long) '1'); 
static DECLARE_TASKLET(int_botton2_tasklet, int_tasklet_func, (unsigned long) '2'); 

/****************************************************************************/
/* IRQ handler - fired on interrupt                                         */
/****************************************************************************/
static irqreturn_t r_irq_handler1(int irq, void *dev_id, struct pt_regs *regs) {

  /*  static unsigned long lastseen=0;
    if(jiffies<lastseen+ ms_debounce * HZ / 1000) return IRQ_HANDLED;  // 1 jiffies = 4ms, HZ=250
	lastseen=jiffies;
	*/
	
	disable_irq_nosync(irq_BUTTON1);
	
	tasklet_schedule(&int_botton1_tasklet);
	

    return IRQ_HANDLED;
}



/****************************************************************************/
/* IRQ handler - fired on interrupt                                         */
/****************************************************************************/
static irqreturn_t r_irq_handler2(int irq, void *dev_id, struct pt_regs *regs) {
	
	/*static unsigned long  lastseen=0;
    if(jiffies<lastseen+ ms_debounce * HZ / 1000) return IRQ_HANDLED;
	lastseen=jiffies;
	*/
	disable_irq_nosync(irq_BUTTON2);
	
	tasklet_schedule(&int_botton2_tasklet);

    return IRQ_HANDLED;
}






/*****************************************************************************/
/* This functions registers devices, requests GPIOs and configures interrupts */
/*****************************************************************************/


void r_dev_config(void)
{
    int ret;

    /*******************************
     *  register device for leds
     *******************************/

    ret = misc_register(&leds_miscdev);
    if (ret < 0) {
        printk(KERN_ERR "misc_register failed\n");
//		r_cleanup();
        return;
    }

    printk(KERN_NOTICE "  leds_miscdev.minor   =%d\n", leds_miscdev.minor);

    /*******************************
    *  register device for buttons
    *******************************/

    ret = misc_register(&buttons_miscdev);
    if (ret < 0) {
        printk(KERN_ERR "misc_register failed\n");
//		r_cleanup();
        return;
    }

    printk(KERN_NOTICE "  buttons_miscdev.minor=%d\n", buttons_miscdev.minor);

    /*******************************
    *  register device for speaker
    *******************************/

    ret = misc_register(&speaker_miscdev);
    if (ret < 0) {
        printk(KERN_ERR "misc_register failed\n");
//		r_cleanup();
        return;
    }

    printk(KERN_NOTICE "  speaker_miscdev.minor=%d\n", speaker_miscdev.minor);

}


void r_GPIO_config(void)
{
    int i;

    /*******************************
     *  request and init gpios for leds
     *******************************/

    for(i=0; i<6; i++)
        if (gpio_request_one(LED_GPIOS[i], GPIOF_INIT_LOW, led_desc[i])) {
            printk("GPIO request faiure: led GPIO %d %s\n",LED_GPIOS[i], led_desc[i]);
            return ;
        }

    /// SPEAKER GPIO:
    if (gpio_request(GPIO_SPEAKER, "GPIO FOR SPEAKER")) {
        printk("GPIO request faiure: %d  %s\n",GPIO_SPEAKER, "GPIO FOR SPEAKER");
        return;
    }

}

void r_int_config(void)
{
    /*******************************
     *  set interrup for button 1
     *******************************/

    if (gpio_request(GPIO_BUTTON1, GPIO_BUTTON1_DESC)) {
        printk("GPIO request faiure: %s\n", GPIO_BUTTON1_DESC);
        return;
    }

    if ( (irq_BUTTON1 = gpio_to_irq(GPIO_BUTTON1)) < 0 ) {
        printk("GPIO to IRQ mapping faiure %s\n", GPIO_BUTTON1_DESC);
        return;
    }

     printk(KERN_NOTICE "  Mapped int %d for button1 in gpio %d\n", irq_BUTTON1, GPIO_BUTTON1);


    if (request_irq(irq_BUTTON1,
                    (irq_handler_t ) r_irq_handler1,
                    IRQF_TRIGGER_FALLING,
                    GPIO_BUTTON1_DESC,
                    GPIO_BUTTON1_DEVICE_DESC)) {
        printk("Irq Request failure\n");
        return;
    }

    /*******************************
     *  set interrup for button 2
     *******************************/

    if (gpio_request(GPIO_BUTTON2, GPIO_BUTTON2_DESC)) {
        printk("GPIO request faiure: %s\n", GPIO_BUTTON2_DESC);
        return;
    }

    if ( (irq_BUTTON2 = gpio_to_irq(GPIO_BUTTON2)) < 0 ) {
        printk("GPIO to IRQ mapping faiure %s\n", GPIO_BUTTON2_DESC);
        return;
    }

    printk(KERN_NOTICE "  Mapped int %d for button2 in gpio %d\n", irq_BUTTON2, GPIO_BUTTON2);

    if (request_irq(irq_BUTTON2,
                    (irq_handler_t ) r_irq_handler2,
                    IRQF_TRIGGER_FALLING,
                    GPIO_BUTTON2_DESC,
                    GPIO_BUTTON2_DEVICE_DESC)) {
        printk("Irq Request failure\n");
        return;
    }

    return;
}


/****************************************************************************/
/* This functions releases interrupts, etc.                                 */
/****************************************************************************/
void r_int_release(void) {

    free_irq(irq_BUTTON1, GPIO_BUTTON1_DEVICE_DESC);
    free_irq(irq_BUTTON2, GPIO_BUTTON2_DEVICE_DESC);
    gpio_free(GPIO_BUTTON1);
    gpio_free(GPIO_BUTTON2);

    return;
}


void r_GPIO_release(void) {
    int i;
    for(i=0; i<6; i++) gpio_free(LED_GPIOS[i]);
    gpio_free(GPIO_SPEAKER);
    return;
}


void r_dev_release(void) {

    if (leds_miscdev.this_device)
        misc_deregister(&leds_miscdev);

    if (buttons_miscdev.this_device)
        misc_deregister(&buttons_miscdev);

    if (speaker_miscdev.this_device)
        misc_deregister(&speaker_miscdev);
}

/****************************************************************************/
/* Module init / cleanup block.                                             */
/****************************************************************************/
int r_init(void) {

    printk(KERN_NOTICE "Hello, loading %s module!\n",KBUILD_MODNAME);

    printk(KERN_NOTICE "Placa - Interrupt config\n");
    r_int_config();

    printk(KERN_NOTICE "  debounce set to: %d ms, %d jiffies\n", ms_debounce, ms_debounce*HZ/1000);

    printk(KERN_NOTICE "Placa - devices config\n");
    r_dev_config();

    printk(KERN_NOTICE "Placa - GPIO config\n");
    r_GPIO_config();
    
    //init_wait_queue_head(&read_queue);

    return 0;
}



void r_cleanup(void) {
    int i;
    printk(KERN_NOTICE "Placa DAC module cleaning up...\n");
    for(i=0; i<6; i++) gpio_set_value(LED_GPIOS[i], 0);
    r_int_release();
    r_GPIO_release();
    r_dev_release();
    printk(KERN_NOTICE "Done. Bye\n");

    return;
}


module_init(r_init);
module_exit(r_cleanup);


/****************************************************************************/
/* Module licensing/description block.                                      */
/****************************************************************************/
MODULE_LICENSE("GPL");
MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
