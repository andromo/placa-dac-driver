MODULE=driver_Placa_DAC
 
KERNEL_SRC=/lib/modules/3.12-1-rpi/build
 
obj-m += ${MODULE}.o
 
compile:
	make -C ${KERNEL_SRC} M=${CURDIR} modules

install: 
	sudo insmod ${MODULE}.ko ms_debounce=300
	dmesg | tail 
	sudo chmod go+rw /dev/leds
	sudo chmod go+rw /dev/buttons
	sudo chmod go+rw /dev/speaker

uninstall:
	sudo rmmod ${MODULE} 
	dmesg | tail
 
 
 
